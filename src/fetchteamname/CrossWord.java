/**
 * Generates CrossWord Puzzles for the GUI.
 * NOT FINISHED (2nd Iteration)
 * Should probably be redone with cells but it honestly works quite well the way it is, its very efficient.
 * 
 * @author Zach Harsh
 * @author Justin Kuzma
 * @author Stephen Gargano
 * @author Rich Prendergast
 * @author Chris Smirga
 */

package fetchteamname;

import java.io.*;
import java.util.*;

public class CrossWord implements Serializable {

	private static final long serialVersionUID = -1727064770935943392L;

	private Random r;

	private char[][] puzzle;
	private char[][] vPuzzle;
	private char[][] hPuzzle;
	private String description;
	private String[] words;

	/**
	 * Constructor for the CrossWord class. This class takes in a string of
	 * words and a size to form the puzzle with. This class is only responsible
	 * for creating the actual puzzle and nothing else. Iterates through each
	 * word and calls the insertWord method. This results in the the actual
	 * puzzle displayed by the GUI. There does not need to be a solution puzzle
	 * because "empty cells" aka black spaces are signified by the # character.
	 * 
	 * @see insertWord()
	 */
	public CrossWord(String[] wordList, int size) {
		words = wordList;
		puzzle = new char[size][size];
		vPuzzle = new char[size][size];
		hPuzzle = new char[size][size];
		description = "";
		r = new Random();

		for (int i = 0; i < puzzle.length; i++) {
			for (int j = 0; j < puzzle.length; j++) {
				puzzle[i][j] = '#';
				vPuzzle[i][j] = '#';
				hPuzzle[i][j] = '#';
			}
		}

		// Sort all the words by length, descending.
		Collections.sort(Arrays.asList(wordList), new sort());

		// Potentially Swap largest word with second largest (Double the possibilities! Woah! Double Rainbow! No way.)
		
		try {
			boolean direction = r.nextBoolean();
			if (direction == true) {
				String temp = wordList[0];
				wordList[0] = wordList[1];
				wordList[1] = temp;
			}
			System.out.println(wordList[0] + " swapped positions with " + wordList[1]);
		}
		catch (Exception e) {
		}
		 

		// Create a new Array
		String[] newList = new String[wordList.length - 1];
		for (int i = 1; i < wordList.length; i++) {
			newList[i - 1] = wordList[i];
		}

		// Take the first word and place it on the board at a random spot thats
		// nearly centered.
		int randOffsetHorizontal = -2 + (int) (Math.random() * 2);
		int randOffsetVertical = -2 + (int) (Math.random() * 2);
		boolean direction = r.nextBoolean();
		for (int i = 0; i < wordList[0].length(); i++) {
			if (direction == true) {
				// Place Vertical
				vPuzzle[size / 2 + randOffsetHorizontal][size / 2 - wordList[0].length() / 2 + randOffsetVertical + i] = wordList[0].charAt(i);
			}
			else {
				// Place Horizontal
				hPuzzle[size / 2 - wordList[0].length() / 2 + randOffsetHorizontal + i][size / 2 + randOffsetVertical] = wordList[0].charAt(i);
			}
		}
		if (direction == true) {
			System.out.println("word " + wordList[0] + " added vertically at " + (size / 2 + randOffsetHorizontal) + ", " 
					+ (size / 2 - wordList[0].length() / 2 + randOffsetVertical));
			description = description + (size / 2 + randOffsetHorizontal) + "," + (size / 2 - wordList[0].length() / 2 + randOffsetVertical) + 
					": description of the word|";
		}
		else {
			System.out.println("word " + wordList[0] + " added horizontally at " + (size / 2 - wordList[0].length() / 2 + randOffsetHorizontal) + 
					", " + (size / 2 + randOffsetVertical));
			description = description + (size / 2 - wordList[0].length() / 2 + randOffsetHorizontal) + "," + (size / 2 + randOffsetVertical) + 
					": description of the word|";
		}

		// Randomize the rest of the array so that there are different solutions
		Collections.shuffle(Arrays.asList(newList));
		
		// Take the next word and attempt to place it
		insertWord(newList);
	}

	/**
	 * Responsible for inserting the next word into the puzzle. It will iterate
	 * through the 2d character array and find a suitable position to place the
	 * word it has been given. Currently this is just a modified version of the
	 * WordSearch Algorithm and is not correct or complete. Empty cells are
	 * represented by the # character.
	 * 
	 * FEW ERRORS RELATED TO WORDS OVERLAPPING AND REPLACING ORIGINAL WORDS
	 * LETTER
	 */

	private void insertWord(String[] newList) {
		char[][] originalPuzzle = new char[puzzle.length][puzzle.length];
		boolean addWord = true;
		boolean wordAdded = false;
		
		for (int r = 0; r < newList.length; r++) {
			wordAdded = false;
			for (int i = 0; i < puzzle.length; i++) {
				for (int j = 0; j < puzzle.length; j++) {
					originalPuzzle[i][j] = puzzle[i][j];
				}
			}
			// Holy nested for loops batman
			for (int i = 0; i < puzzle.length; i++) {
				for (int j = 0; j < puzzle.length; j++) {
					for (int k = 0; k < newList[r].length(); k++) {
						//The insert if a vertical one
						if (hPuzzle[i][j] == newList[r].charAt(k)) {
							try {
								addWord = true;
								if (vPuzzle[i][j - k - 1] == '#'
										&& vPuzzle[i][j + (newList[r].length() - k)] == '#'
										&& hPuzzle[i][j - k - 1] == '#'
										&& hPuzzle[i][j + (newList[r].length() - k)] == '#'
										&& vPuzzle[i][j - 1] == '#'
										&& vPuzzle[i][j + 1] == '#') {
									for (int p = 0; p < newList[r].length(); p++) {
										if (vPuzzle[i - k + p][j] != newList[r].charAt(p) && hPuzzle[i - k + p][j] != newList[r].charAt(p)) {
											if ((vPuzzle[i - 1][j - k + p] != '#' || vPuzzle[i + 1][j - k + p] != '#')
												|| (hPuzzle[i - 1][j - k + p] != '#' || hPuzzle[i + 1][j - k + p] != '#')) {
												addWord = false;
											}
										}
									}
									if (addWord && !wordAdded) {
										for (int z = 0; z < newList[r].length(); z++) {
											if (i < 0 || i > vPuzzle[0].length) {
												// Runs off the edge horizontally, do something
												// break;
											}
											else {
												// Place Characters
												vPuzzle[i][j - k + z] = newList[r].charAt(z);
											}
										}
										wordAdded=true;
										addWord = false;
										
										System.out.println("word " + newList[r] + " added vertically at " + (i+1) + ", " + (j-k+1));
										description = description + (i+1) + "," + (j-k+1) + ": description of the word|";
										// break;
									} 
								}
							}
							catch (ArrayIndexOutOfBoundsException e) {
								// We ran out of bounds bro
							}
						}
						//The insert is a horizontal one
						else if (vPuzzle[i][j] == newList[r].charAt(k)) {
							try {
								if (hPuzzle[i - k - 1][j] == '#'
										&& hPuzzle[i + (newList[r].length() - k)][j] == '#'
										&& hPuzzle[i - 1][j] == '#'
										&& hPuzzle[i + 1][j] == '#'
										&& vPuzzle[i - k - 1][j] == '#'
										&& vPuzzle[i + (newList[r].length() - k)][j] == '#') {
									for (int p = 0; p < newList[r].length(); p++) {
										if (hPuzzle[i - k + p][j] != newList[r].charAt(p) && vPuzzle[i - k + p][j] != newList[r].charAt(p)) {
											if ((hPuzzle[i - k + p][j - 1] != '#' || hPuzzle[i - k + p][j + 1] != '#')
												|| (vPuzzle[i - k + p][j - 1] != '#' || vPuzzle[i - k + p][j + 1] != '#')) {
												addWord = false;
											}
										}
									}
									if (addWord && !wordAdded) {
										for (int z = 0; z < newList[r].length(); z++) {
											if (i < 0 || i > hPuzzle[0].length) {
												// Runs off the edge horizontally, do something
												// break;
											}
											else {
												// Place Characters
												hPuzzle[i - k + z][j] = newList[r].charAt(z);
											}
										}
										addWord = false;
										wordAdded=true;
										
										System.out.println("word " + newList[r] + " added horizontally at " + (i-k+1) + ", " + (j+1));
										description = description + (i-k+1) + "," + (j+1) + ": description of the word|";
										// break;
									}
								}
							}
							catch (ArrayIndexOutOfBoundsException e) {
								// We ran out of bounds bro
							}
						}
						else {
							/*
							 * No shared character, skip to next word (ideally
							 * you'd place this word back into the list.
							 */
						}
					}
				}
			}
			if (!wordAdded) {
				System.out.println("Word could not be added: " + newList[r].toString());
			}
		}
		combinePuzzles();
	}
	
	/**
	 * Combines the horizontal and vertical puzzles into one puzzle
	 */
	private void combinePuzzles() {
		for (int i = 0; i < puzzle.length; i++) {
			for (int j = 0; j < puzzle.length; j++) {
				if (vPuzzle[i][j] != '#') {
					puzzle[i][j] = vPuzzle[i][j];
				}
				if (hPuzzle[i][j] != '#') {
					puzzle[i][j] = hPuzzle[i][j];
				}
			}
		}
	}
	
	/**
	 * Returns the puzzle
	 */
	public char[][] getPuzzle() {
		return puzzle;
	}
	
	/**
	 * Returns the array of words that are used in this puzzle
	 * 
	 * @return      puzzle words
	 */
	public String[] getWords() {
		return words;
	}
	
	/**
	 * Returns the descriptions of the words as a single String
	 * 
	 * @return      puzzle descriptions
	 */
	public String getDescriptions() {
		return description;
	}
}

/**
 * Comparator to sort strings by length
 */
class sort implements Comparator<String> {
	public int compare(String wordOne, String wordTwo) {
		if (wordOne.length() > wordTwo.length()) {
			return -1;
		}
		else if (wordOne.length() < wordTwo.length()) {
			return 1;
		}
		else {
			return 0;
		}
	}
}
