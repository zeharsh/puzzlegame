/**
* GUI of the Puzzle Game. Responsible for drawing a Puzzle on the Frame as well
* as for the menu bar. Handles all actions that are part of the menu and any possible
* mouse or keyboard entry as well. For now this is both the controller and the view,
* but we will change this later on most likely.
* 
* @author Zach Harsh
* @author Justin Kuzma
* @author Stephen Gargano
* @author Rich Prendergast
* @author Chris Smirga
*/

package fetchteamname;

import java.awt.*;
import java.awt.event.*;
import java.awt.image.BufferedImage;
import java.io.*;
import java.net.URI;
import java.net.URISyntaxException;
import java.util.*;

import javax.imageio.ImageIO;
import javax.swing.*;

public class PuzzleGUI extends JPanel implements ActionListener, WindowListener {
	
	private static final long serialVersionUID = -4891417203658431858L;
	private String osName = "";
	
	private JFrame jf;
	private static final String title = "Puzzle Creator";
	
	private static final String newMenuText = "New Puzzle";
	private static final String saveWordsMenuText = "Save Words";
	private static final String savePuzzleMenuText = "Save Puzzle";
	private static final String openPuzzleMenuText = "Load Puzzle";
	private static final String openWordsMenuText = "Load Words";
	private static final String generateHTML = "Generate HTML";
	private static final String exitText = "Exit";
	private static final String randomizeButtonText = "Randomize";
	private static final String findButtonText = "Find Word";
	private static final String showSolutionText = "Toggle Solution";
	private static final String PuzzleTypeText = "Toggle Puzzle Type";
	private static final String instructionsText = "Instructions";
	private static final String aboutText = "About";
	
	private int defaultPuzzleSize = 20; //Default = 20
	private int puzzleSize = 20;
	private int cellSize = 32; //Default = 32
	
	private WordSearch ws;
	private CrossWord cw;
	private String[] currentPuzzleWords;
	
	private final int CrossWord = 0;
	private final int WordSearch = 1;
	
	private int puzzleType = WordSearch;
	private boolean puzzleLoaded = false;
	private boolean showSolutions = false;
	
	/**
	 * Constructor of the PuzzleGUI class. The constructor is only responsible
	 * for setting up the JFrame and some other odds and ends depending on the
	 * Operating System that the user is running this application on. It will
	 * invoke the createMenu() method to create the menu followed by the
	 * startGame() method to finish up anything else that it may need to do before
	 * an actual game is loaded or created.
	 * 
	 * @see	createMenu()
	 * @see	startGame()
	 */
	public PuzzleGUI() {
		System.setProperty("com.apple.mrj.application.apple.menu.about.name", "Puzzle Creator");
		System.setProperty("apple.laf.useScreenMenuBar", "true");
		
		jf = new JFrame(title);
		jf.setSize(360, 320);
		jf.setResizable(false);
		Dimension dim = Toolkit.getDefaultToolkit().getScreenSize();
		jf.setLocation(dim.width/2 - jf.getSize().width/2, dim.height/2 - jf.getSize().height/2);
		jf.setDefaultCloseOperation(JFrame.DO_NOTHING_ON_CLOSE); 
		jf.addWindowListener(this);
		
		createMenu(jf);
		jf.add(this);
		startGame();
		
		jf.setVisible(true);
	}
	
	/**
	 * Creates the Menu Bar for the Application. This will appear on
	 * Menu Bar on OSX and in the actual Application on Windows/Linux.
	 *
	 * @param  frame The parent JFrame this menu is to be placed in.
	 */
	private void createMenu(JFrame frame) {
		JMenuBar menuBar = new JMenuBar();
		frame.setJMenuBar(menuBar);
				
		JMenu fileMenu = new JMenu("File");
		menuBar.add(fileMenu);
		
		JMenuItem newPuzzle = new JMenuItem(newMenuText);
		newPuzzle.addActionListener(this);
		newPuzzle.setAccelerator(KeyStroke.getKeyStroke(java.awt.event.KeyEvent.VK_N, Toolkit.getDefaultToolkit().getMenuShortcutKeyMask()));
		fileMenu.add(newPuzzle);
		
		JMenuItem openCreatedPuzzle = new JMenuItem(openPuzzleMenuText);
		openCreatedPuzzle.addActionListener(this);
		openCreatedPuzzle.setAccelerator(KeyStroke.getKeyStroke(KeyEvent.VK_O, ActionEvent.ALT_MASK));
		fileMenu.add(openCreatedPuzzle);
		
		JMenuItem saveCreatedPuzzle = new JMenuItem(savePuzzleMenuText);
		saveCreatedPuzzle.addActionListener(this);
		saveCreatedPuzzle.setAccelerator(KeyStroke.getKeyStroke(KeyEvent.VK_S, ActionEvent.ALT_MASK));
		fileMenu.add(saveCreatedPuzzle);
		
		JMenuItem openPuzzle = new JMenuItem(openWordsMenuText);
		openPuzzle.addActionListener(this);
		openPuzzle.setAccelerator(KeyStroke.getKeyStroke(java.awt.event.KeyEvent.VK_O, Toolkit.getDefaultToolkit().getMenuShortcutKeyMask()));
		fileMenu.add(openPuzzle);
		
		JMenuItem savePuzzleWords = new JMenuItem(saveWordsMenuText);
		savePuzzleWords.addActionListener(this);
		savePuzzleWords.setAccelerator(KeyStroke.getKeyStroke(java.awt.event.KeyEvent.VK_S, Toolkit.getDefaultToolkit().getMenuShortcutKeyMask()));
		fileMenu.add(savePuzzleWords);
		
		JMenuItem makeHTML = new JMenuItem(generateHTML);
		makeHTML.addActionListener(this);
		makeHTML.setAccelerator(KeyStroke.getKeyStroke(java.awt.event.KeyEvent.VK_H, Toolkit.getDefaultToolkit().getMenuShortcutKeyMask()));
		fileMenu.add(makeHTML);
		
		JMenuItem endGame = new JMenuItem(exitText);
		endGame.addActionListener(this);
		endGame.setAccelerator(KeyStroke.getKeyStroke(java.awt.event.KeyEvent.VK_Q, Toolkit.getDefaultToolkit().getMenuShortcutKeyMask()));
		fileMenu.add(endGame);
		
		JMenu actionMenu = new JMenu("Action");
		menuBar.add(actionMenu);
		
		JMenuItem toggleType = new JMenuItem(PuzzleTypeText);
		toggleType.addActionListener(this);
		toggleType.setAccelerator(KeyStroke.getKeyStroke(java.awt.event.KeyEvent.VK_T, Toolkit.getDefaultToolkit().getMenuShortcutKeyMask()));
		actionMenu.add(toggleType);
		
		JMenuItem randomize = new JMenuItem(randomizeButtonText);
		randomize.addActionListener(this);
		randomize.setAccelerator(KeyStroke.getKeyStroke(java.awt.event.KeyEvent.VK_R, Toolkit.getDefaultToolkit().getMenuShortcutKeyMask()));
		actionMenu.add(randomize);

		JMenuItem findWord = new JMenuItem(findButtonText);
		findWord.addActionListener(this);
		actionMenu.add(findWord);
		
		JMenuItem showSolution = new JMenuItem(showSolutionText);
		showSolution.addActionListener(this);
		showSolution.setAccelerator(KeyStroke.getKeyStroke(java.awt.event.KeyEvent.VK_A, Toolkit.getDefaultToolkit().getMenuShortcutKeyMask()));
		actionMenu.add(showSolution);
		
		JMenu helpMenu = new JMenu("Help");
		menuBar.add(helpMenu);
		
		JMenuItem instructionsGame = new JMenuItem(instructionsText);
		instructionsGame.addActionListener(this);
		helpMenu.add(instructionsGame);
		
		JMenuItem aboutGame = new JMenuItem(aboutText);
		aboutGame.addActionListener(this);
		helpMenu.add(aboutGame);
	}
	
	/**
	 * Responsible for tasks before anything else happens. Currently
	 * it is responsible only for identifying which Operating System
	 * this program is running on.
	 */
	private void startGame() {
		osName = System.getProperty("os.name");
		String[] words = {"FETCH", "TEAM","NAME"};
		currentPuzzleWords = words;
		puzzleSize = 10;
		createPuzzle();
		showSolutions = true;
	}
	
	/**
	 * Positions the Frame in the middle of the user's screen. Also
	 * responsible for repainting the frame's contents.
	 */
	private void positionFrame() {
		if (osName.equals("Mac OS X")) jf.setSize(puzzleSize*cellSize, puzzleSize*cellSize+22);
		else jf.setSize(puzzleSize*cellSize+7, puzzleSize*cellSize+52);
		Dimension dim = Toolkit.getDefaultToolkit().getScreenSize();
		jf.setLocation(dim.width/2 - jf.getSize().width/2, dim.height/2 - jf.getSize().height/2);
		jf.repaint(1000);
	}
	
	/**
	 * Returns a File name that the user has selected. This includes the
	 * full path information as well.
	 *
	 * @return      the file name (full path)
	 */
	public String obtainFileName() {
		String theFileName = null;
		final FileDialog fc = new FileDialog(jf);
		fc.setMode(FileDialog.LOAD);
		Dimension dim = Toolkit.getDefaultToolkit().getScreenSize();
		fc.setLocation(dim.width/2 - fc.getSize().width/2, dim.height/2 - fc.getSize().height/2);
		fc.setVisible(true);
		theFileName = fc.getDirectory() + fc.getFile();
		
		if (fc.getFile() != null) {
			jf.setTitle(title + " - " + fc.getFile());
			System.out.println("Selected File: " + theFileName);
		}
		else {
			System.out.println("No File Selected");
		}
		
  		return theFileName;
	}
	
	/**
	 * Saves the current puzzle instance into a file.
	 * The user picks the directory and name for the file.
	 */
	private void savePuzzleState() {
		String theFileName = null;
		final FileDialog fc = new FileDialog(jf);
		fc.setMode(FileDialog.SAVE);
		fc.setFile("*.puzz");
		Dimension dim = Toolkit.getDefaultToolkit().getScreenSize();
		fc.setLocation(dim.width/2 - fc.getSize().width/2, dim.height/2 - fc.getSize().height/2);
		fc.setVisible(true);
		theFileName = fc.getDirectory() + fc.getFile();
		theFileName = theFileName.endsWith(".puzz") ? theFileName : theFileName + ".puzz";
		try {
			ObjectOutputStream puzzle = new ObjectOutputStream(new FileOutputStream(theFileName));
			if (puzzleType == CrossWord) {
				puzzle.writeInt(CrossWord);
				puzzle.writeObject(cw);
			}
			else if (puzzleType == WordSearch) {
				puzzle.writeInt(WordSearch);
				puzzle.writeObject(ws);
			}
			puzzle.close();
		}
		catch (Exception e) {
			System.out.println("I herped my derp bro");
		}
		
	}
	
	/**
	 * Loads a puzzle instance from a file.
	 */
	private void loadPuzzleState() {
		try {
			FileInputStream puzzle = new FileInputStream(new File(obtainFileName()));
			ObjectInputStream puzzleLoader = new ObjectInputStream(puzzle);
			puzzleSize = defaultPuzzleSize;
			puzzleType = puzzleLoader.readInt();
			if (puzzleType == CrossWord) {
				System.out.println("Puzzle was a CrossWord");
				cw = (CrossWord) puzzleLoader.readObject();
				currentPuzzleWords = cw.getWords();
			}
			else if (puzzleType == WordSearch) {
				System.out.println("Puzzle was a WordSearch");
				ws = (WordSearch) puzzleLoader.readObject();
				currentPuzzleWords = ws.getWords();
			}
			else {
				System.out.println("Not a valid Puzzle Type");
			}
			positionFrame();
			puzzleLoader.close();
		}
		catch (Exception e) {
			System.out.println("I herped my derp bro");
		}
	}
	
	/**
	 * Loads words saved in a .csv file or any other file that uses
	 * comma separated data. Invokes the obtainFileName method to get
	 * the full path of the file selected.
	 * 
	 * @see	obtainFileName()
	 */
	public void loadPuzzleWords() {
		Scanner scanner = null;

		try {
			scanner = new Scanner(new File(obtainFileName()));
		}
		catch (FileNotFoundException e) {
			currentPuzzleWords = null;
			return;
		}

		StringBuilder wordList = new StringBuilder();
		while (scanner.hasNext()) {
			wordList.append(scanner.next().toUpperCase() + ",");
		}
		scanner.close();

		wordList.deleteCharAt(wordList.length() - 1);
		currentPuzzleWords = wordList.toString().split(",");
		showSolutions = false;
		
		puzzleSize = defaultPuzzleSize;
		createPuzzle();
	}
	
	/**
	 * Saves the current puzzle words into a .csv file as comma
	 * separated text. The user picks the directory and name for
	 * the file.
	 */
	private void savePuzzleWords() {
		String theFileName = null;
		final FileDialog fc = new FileDialog(jf);
		fc.setMode(FileDialog.SAVE);
		fc.setFile("*.csv");
		Dimension dim = Toolkit.getDefaultToolkit().getScreenSize();
		fc.setLocation(dim.width/2 - fc.getSize().width/2, dim.height/2 - fc.getSize().height/2);
		fc.setVisible(true);
		theFileName = fc.getDirectory() + fc.getFile();
		theFileName = theFileName.endsWith(".csv") ? theFileName : theFileName + ".csv";
		BufferedWriter writer = null;
		try {
			writer = new BufferedWriter(new FileWriter(theFileName));
			for (int i = 0; i < currentPuzzleWords.length; i++) {
				if (i < currentPuzzleWords.length - 1) writer.write(currentPuzzleWords[i] + ",");
				else writer.write(currentPuzzleWords[i]);
				writer.flush();
			}
		} 
		catch (IOException ex) {
			ex.printStackTrace();
		}
		try {
			writer.close();
		}
		catch (IOException e) {
			e.printStackTrace();
		}
	}
	
	/**
	 * Creates a dialog asking for comma separated words that will be
	 * used in a new puzzle. Invokes createPuzzle to form the puzzle and
	 * display them on screen after you have finished entering words.
	 * 
	 * @see	createPuzzle()
	 */
	private void newPuzzle() {
		String enteredWords = "";
		boolean valid = false;
		puzzleSize = defaultPuzzleSize;
		
		while (!valid) {
			enteredWords = JOptionPane.showInputDialog(jf, "Enter comma separated words below:");
			boolean isWrongWordLength = false;
			boolean isAllLetters = true;
			String wrongLengthWord = "";
			String notAllLettersWord = "";
			String[] enteredWordsCopy = enteredWords.split(",");

			for (int i = 0; i < enteredWordsCopy.length; i++) {
				String word = enteredWordsCopy[i].trim();
				String pattern= "^[a-zA-Z]*$";
				
				if (word.length() < 3 || word.length() > puzzleSize) {
					isWrongWordLength = true;
					wrongLengthWord = enteredWordsCopy[i];
				}

				if (!word.matches(pattern)) {
					isAllLetters = false;
					notAllLettersWord = word;
				}
			}
		
			if (enteredWords.isEmpty()){
				JOptionPane.showMessageDialog(jf, "Error: No words entered.");
			}
			else if (!isAllLetters){
				JOptionPane.showMessageDialog(jf, "Error: \""  + notAllLettersWord + "\"" + " contains characters that are not letters.");
			}
			else if (isWrongWordLength){
				JOptionPane.showMessageDialog(jf, "Error: \"" + wrongLengthWord + "\"" + " has an incorrect length, word length must be between 3 and " + puzzleSize + " letters long.");
			}
			else {
				valid = true;
			}
		}
		
		enteredWords = enteredWords.toUpperCase();
		enteredWords = enteredWords.trim();
		currentPuzzleWords = enteredWords.split(",[ ]*");
		puzzleSize = defaultPuzzleSize;
		createPuzzle();

	}
	
	/**
	 * Generates an HTML file with either a CrossWord or a WordSearch
	 * puzzle and any additional information required such as words that
	 * are in this puzzle or their location and/or their descriptions.
	 */
	public void saveHTML() {
		String theFileName = null;
		final FileDialog fc = new FileDialog(jf);
		fc.setMode(FileDialog.SAVE);
		fc.setFile("*.html");
		Dimension dim = Toolkit.getDefaultToolkit().getScreenSize();
		fc.setLocation(dim.width/2 - fc.getSize().width/2, dim.height/2 - fc.getSize().height/2);
		fc.setVisible(true);
		theFileName = fc.getDirectory() + fc.getFile();
		theFileName = theFileName.endsWith(".html") ? theFileName : theFileName + ".html";
		
		BufferedImage bufImage = new BufferedImage(getSize().width, getSize().height,BufferedImage.TYPE_INT_ARGB_PRE);
	    paint(bufImage.createGraphics());
	    File imageFile = new File(fc.getDirectory() + "temp.png");
		try{
			imageFile.createNewFile();
			ImageIO.write(bufImage, "png", imageFile);
		}
		catch(Exception e){
			
		}
		
		PrintWriter nullWriter;
		try {
			nullWriter = new PrintWriter(new File(theFileName));
			nullWriter.print("");
			nullWriter.close();
		}
		catch (FileNotFoundException e) {}
		BufferedWriter writer = null;
		try {
			writer = new BufferedWriter(new FileWriter(theFileName));
			writer.write("<!DOCTYPE html>");
			writer.newLine();
			writer.write("<html>");
			writer.newLine();
			writer.write("<body>");
			writer.newLine();
			writer.write("<h1>" + fc.getFile() + "</h1>");
			writer.newLine();
			writer.write("<p>");
			writer.newLine();
			if (puzzleType == WordSearch) {
				writer.write("<img src=\"temp.png\" alt=\"WordSearch Puzzle\">");
				String[] descriptions = ws.getWords();
				for (int i = 0; i < descriptions.length; i++) {
					writer.newLine();
					writer.write("<p>");
					writer.write(descriptions[i]);
					writer.write("</p>");
				}
			}
			else if (puzzleType == CrossWord) {
				writer.write("<img src=\"temp.png\" alt=\"CrossWord Puzzle\">");
				String[] descriptions = cw.getDescriptions().split("\\|");
				for (int i = 0; i < descriptions.length; i++) {
					writer.newLine();
					writer.write("<p>");
					writer.write(descriptions[i]);
					writer.write("</p>");
				}
			}
			writer.newLine();
			writer.write("</p>");
			writer.newLine();
			writer.write("</body>");
			writer.newLine();
			writer.write("</html>");
		} 
		catch (IOException ex) {
			ex.printStackTrace();
		}
		try {
			writer.close();
			Desktop.getDesktop().open(new File(theFileName));
			jf.repaint(1000);
		}
		catch (IOException e) {
			e.printStackTrace();
		}
	}
	
	/**
	 * Toggles the type of the Puzzle. For now this toggles between a
	 * WordSearch puzzle and a CrossWord puzzle. Future puzzle types could be
	 * implemented with a slight modification to this method.
	 */
	private void togglePuzzleType() {
		if (puzzleType == CrossWord) puzzleType = WordSearch;
		else puzzleType = CrossWord;
		createPuzzle();
	}
	
	/**
	 * Takes the current array of words to be used in a new puzzle and
	 * invokes the WordSearch class to create a new puzzle. It will then
	 * invoke positionFrame to recenter the window and display the puzzle
	 * that was generated.
	 * 
	 * @see	positionFrame()
	 */
	private void createPuzzle() {
		if (currentPuzzleWords != null) {
			if (puzzleType == WordSearch) {
				showSolutions = true;
				ws = new WordSearch(currentPuzzleWords, puzzleSize);
			}
			if (puzzleType == CrossWord) {
				cw = new CrossWord(currentPuzzleWords, puzzleSize);
				showSolutions = false;
			}
			puzzleLoaded = true;
			positionFrame();
		}
	}
	
	/**
	 * Toggles the showSolutions variable and invokes a repaint.
	 */
	private void toggleSolutions() {
		showSolutions = !showSolutions;
		jf.repaint(1000);
	}
	
	/**
	 * Opens up an online instructions manual for how to use this
	 * application in the default web browser installed on the system.
	 */
	private void openInstructions() {
		String url = "http://zeharsh.info/files/Manual.pdf";
        if (Desktop.isDesktopSupported()) {
            try {
				Desktop.getDesktop().browse(new URI(url));
			}
            catch (IOException e1) {
				e1.printStackTrace();
			}
            catch (URISyntaxException e1) {
				e1.printStackTrace();
			}
        }
        else {
            Runtime runtime = Runtime.getRuntime();
            try {
				runtime.exec("/usr/bin/firefox -new-window " + url);
			}
            catch (IOException e1) {
				e1.printStackTrace();
			}
        }
	}
	
	/**
	 * This method is responsible for all of the painted items within
	 * the Frame on the Panel. It draws a grid and if a puzzle has been 
	 * loaded it will paint the current puzzle. If the showSolutions variable 
	 * is toggled on, it will also highlight all of the solutions in the puzzle 
	 * with a yellow background.
	 *
	 * @param  g	Graphics g
	 */
	public void paintComponent(Graphics g) {
		g.setColor(Color.BLACK);
		if (puzzleLoaded) {
			if (puzzleType == WordSearch) {
				for (int i = 0; i < puzzleSize; i++) {
					for (int j = 0; j < puzzleSize; j++) {
						g.drawRect(i*cellSize, j*cellSize, cellSize, cellSize);
						g.drawString("" + ws.puzzle[i][j], i*cellSize + (cellSize/2) - 2, j*cellSize + (cellSize/2) + 2);
					}
				}
				if (showSolutions) {
					for (int i = 0; i < puzzleSize; i++) {
						for (int j = 0; j < puzzleSize; j++) {
							if (ws.puzzle[i][j] == ws.solution[i][j]) {
								g.setColor(Color.YELLOW);
								g.fillRect(i*cellSize, j*cellSize, cellSize, cellSize);
								g.setColor(Color.BLACK);
								g.drawRect(i*cellSize, j*cellSize, cellSize, cellSize);
							}
							g.drawString("" + ws.solution[i][j], i * cellSize + (cellSize/2) - 2, j * cellSize + (cellSize/2) + 2);
						}
					}
				}
			}
			else if (puzzleType == CrossWord) {
				for (int i = 0; i < puzzleSize; i++) {
					for (int j = 0; j < puzzleSize; j++) {
						if (cw.getPuzzle()[i][j] != '#') g.drawRect(i*cellSize, j*cellSize, cellSize, cellSize);
						else g.fillRect(i*cellSize, j*cellSize, cellSize, cellSize);
						g.drawString("" + cw.getPuzzle()[i][j], i*cellSize + (cellSize/2) - 2, j*cellSize + (cellSize/2) + 2);
					}
				}
				if (showSolutions) {
					for (int i = 0; i < puzzleSize; i++) {
						for (int j = 0; j < puzzleSize; j++) {
							if (cw.getPuzzle()[i][j] != '#') {
								g.setColor(Color.WHITE);
								g.fillRect(i*cellSize, j*cellSize, cellSize, cellSize);
								g.setColor(Color.BLACK);
								g.drawRect(i*cellSize, j*cellSize, cellSize, cellSize);
							}
							else {
								g.setColor(Color.BLACK);
								g.fillRect(i*cellSize, j*cellSize, cellSize, cellSize);
							}
						}
					}
				}
			}
		}
	}
	
	/**
	 * Responsible for any actions in the frame, mostly just the Menu
	 * Bar. It will choose an appropriate action based on which menu item
	 * was selected.
	 */
	public void actionPerformed(ActionEvent e) {
		String command = e.getActionCommand();
		System.out.println(command);
		if (command == newMenuText) {
			newPuzzle();
		}
		else if (command == openPuzzleMenuText) {
			loadPuzzleState();
		}
		else if (command == savePuzzleMenuText) {
			savePuzzleState();
		}
		else if (command == openWordsMenuText) {
			loadPuzzleWords();
		}
		else if (command == saveWordsMenuText) {
			savePuzzleWords();
		}
		else if (command == generateHTML) {
			saveHTML();
		}
		else if (command == exitText) {
			exitGameDialog();
		}
		else if (command == PuzzleTypeText) {
			togglePuzzleType();
		}
		else if (command == randomizeButtonText) {
			createPuzzle();
		}
		else if (command == findButtonText) {
			System.out.println("NOT IMPLEMENTED YET");
		}
		else if (command == showSolutionText) {
			toggleSolutions();
		}
		else if (command == instructionsText) {
			openInstructions();
		}
		else if (command == aboutText) {
			aboutWindow();
		}
		else {
			System.out.println("Unknown action: " + command);
		}
		jf.repaint(1000);
	}
	
	/**
	 * Creates a dialog showing who all is responsible for writing this
	 * application.
	 */
	private void aboutWindow() {
		final ImageIcon icon = new ImageIcon(getClass().getResource("/images/Logo.png"));
		JOptionPane.showMessageDialog(null, "Written By:\nZach Harsh\nJustin Kuzma\nStephen Gargano\nRich Prendergast\nChris Smirga", 
				"About Puzzle Creator", JOptionPane.INFORMATION_MESSAGE, icon);
	}
	
	/**
	 * A precautionary method that is invoked when either the user clicks
	 * on the X in the Window or selects the Exit option from the Menu. This
	 * method ensures that the user really meant to exit the application.
	 */
	private void exitGameDialog() {
		if (JOptionPane.showConfirmDialog(null, "Are you sure you want to exit?", "Exit", 
				JOptionPane.YES_NO_OPTION, JOptionPane.QUESTION_MESSAGE) == JOptionPane.YES_OPTION) {
			System.exit(0);
		}
	}
	
	/**
	 * Invokes the exitGameDialog() method to ensure that the user really
	 * wanted to exit the application.
	 * 
	 * @see	exitGameDialog()
	 */
	public void windowClosing(WindowEvent e) {
		exitGameDialog();
	}
	public void windowActivated(WindowEvent e) {}
	public void windowClosed(WindowEvent e) {}
	public void windowDeactivated(WindowEvent e) {}
	public void windowDeiconified(WindowEvent e) {}
	public void windowIconified(WindowEvent e) {}
	public void windowOpened(WindowEvent e) {}

}
