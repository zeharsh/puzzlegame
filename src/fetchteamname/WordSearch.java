/**
* Generates WordSearch Puzzles for the GUI.
* MOSTLY FINISHED
* 
* @author Zach Harsh
* @author Justin Kuzma
* @author Stephen Gargano
* @author Rich Prendergast
* @author Chris Smirga
*/

package fetchteamname;

import java.io.*;
import java.util.*;

public class WordSearch implements Serializable {
	
	private static final long serialVersionUID = 1995356364025366511L;
	
	private Random r = new Random();
	public char[][] puzzle;
	public char[][] solution;
	private String[] words;
	
	private final int horizontal = 0;
	private final int vertical = 1;
	private final int diagonal = 2;
	
	private final int attemptTarget = 100; //Default = 100
	
	private final char[] alphabet = {'A', 'B', 'C', 'D', 'E', 'F', 'G', 'H', 'I', 'J', 
									 'K', 'L', 'M', 'N', 'O', 'P', 'Q', 'R', 'S', 'T', 
									 'U', 'V', 'W', 'X', 'Y', 'Z'};
	
	/**
	 * Constructor for the WordSearch class. This class takes in a string
	 * of words and a size to form the puzzle with. This class is only
	 * responsible for creating the actual puzzle and nothing else. Iterates
	 * through each word and calls the insertWord method. This results in the
	 * solution puzzle (words with no random filler characters). It will then
	 * create a second final puzzle filled with random characters that is the
	 * actual puzzle displayed by the GUI.
	 * 
	 * @see insertWord()
	 * @see fillWithRandom()
	 */
	public WordSearch(String[] wordList, int size) {
		words = wordList;
		solution = new char[size][size];
		
		for (int i = 0; i < solution.length; i++) {
			for(int j = 0; j < solution.length; j++) {
				solution[i][j] = ' ';
			}
		}
		
		for (String word : wordList) {
			insertWord(word, solution);
		}
		
		puzzle = fillWithRandom(solution);
	}
	
	/**
	 * Responsible for inserting the next word into the puzzle. It will iterate
	 * through the 2d character array and find a suitable position to place the
	 * word it has been given. This algorithm does place words into some other words
	 * but could be improved.
	 */
	private void insertWord(String word, char[][] puzzle) {
		
		char[][] originalPuzzle = new char[puzzle.length][puzzle.length];
		
		for (int i = 0; i < puzzle.length; i++) {
			for (int j = 0; j < puzzle.length; j++) {
				originalPuzzle[i][j] = puzzle[i][j];
			}
		}
		
		int curAttempt = 0;
		while (curAttempt < attemptTarget) {
			curAttempt++;
			Random r = new Random();
			
			if (r.nextInt(2) >= 1) {
				word = reverseString(word);
			}
			
			int direction = r.nextInt(3);
			int row = r.nextInt(puzzle.length - word.length());
			int col	= r.nextInt(puzzle.length - word.length());
			
			int i = 0;
			for (i = 0; i < word.length(); i++) {
				if (puzzle[row][col] == ' ' || puzzle[row][col] == word.charAt(i)) {
					puzzle[row][col] = word.charAt(i);
					if (direction == horizontal) {
						col++;
					}
					else if (direction == vertical) {
						row++;
					}
					else if (direction == diagonal) {
						row++;
						col++;
					}
				}
				else {
					for (int j = i; j > 0; j--) {
						if (direction == horizontal) {
							col--;
						}
						else if (direction == vertical) {
							row--;
						}
						else if (direction == diagonal) {
							row--;
							col--;
						}
						puzzle[row][col] = originalPuzzle[row][col];
					}
					break;
				}
			}
			
			if (--i > 0) {
				return;
			}
		}
	}
	
	/**
	 * Reverses a String so that a word is backwards in the puzzle
	 */
	private String reverseString(String word) {
		StringBuilder reverseWord = new StringBuilder();
		
		for (int i = word.length() - 1; i >= 0; i--) {
			reverseWord.append(word.charAt(i));
		}
		
		return reverseWord.toString();
	}
	
	/**
	 * Fills a constructed puzzle with random characters. The resulting puzzle
	 * is the actual puzzle displayed.
	 * 
	 * @see getRandonChar()
	 */
	private char[][] fillWithRandom(char[][] puzzle) {
		char[][] newPuzzle = new char[puzzle.length][puzzle.length];
		
		for (int i = 0; i < newPuzzle.length; i++) {
			for (int j = 0; j < newPuzzle.length; j++) {
				if (puzzle[i][j] != ' ') {
					newPuzzle[i][j] = puzzle[i][j];
				}
				else {
					newPuzzle[i][j] = getRandomChar();
				}
			}
		}
		
		return newPuzzle;
	}
	
	/**
	 * A simple helper method used to generate a random character for use in the
	 * completed puzzle.
	 */
	public char getRandomChar() {
		return alphabet[r.nextInt(26)];
	}
	
	/**
	 * Returns the array of words that are used in this puzzle
	 * 
	 * @return      puzzle words
	 */
	public String[] getWords() {
		return words;
	}
	
}
