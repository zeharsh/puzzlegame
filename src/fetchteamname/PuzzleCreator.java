/**
* Main Class of the Puzzle Game, doesn't do much.
* 
* @author Zach Harsh
* @author Justin Kuzma
* @author Stephen Gargano
* @author Rich Prendergast
* @author Chris Smirga
*/

package fetchteamname;

public class PuzzleCreator {
	
	/**
	 * This class only exists to ensure that the application is
	 * given a decent title in the menu bar of OSX for now.
	 */
	public static void main(String [] args) {
		new PuzzleGUI();
	}
	
}
