# fetch(team_name); Puzzle Creator

A simple and elegant cross platform Puzzle Creation Tool.

Zach Harsh, Justin Kuzma, Stephen Gargano, Rich Prendergast, Chris Smirga

Javadoc info can be found in /doc/ or http://zeharsh.info/doc/

The aim of this project is to create a simple, yet extremely functional, Puzzle Creator.

It currently has the ability to create and export Crossword and WordSearch Puzzles.

The Third Iteration is complete and 99% of the intended functionality is implemented.